package com.dev.config;


import java.io.FileNotFoundException;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.config.server.EnableConfigServer;

@SpringBootApplication
@EnableConfigServer
public class ConfigServerApplication{
	
    public static void main(String args[]) throws FileNotFoundException {
    	SpringApplication.run(ConfigServerApplication.class, args);
    }
}